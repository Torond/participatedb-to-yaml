# participatedb-to-yaml

Convert ParticipateDB web site to a Git repository of YAML files.


## Usage

```bash
./harvest_participatedb.py html/
./participatedb_to_yaml.py html/ ../participatedb-yaml/
```
